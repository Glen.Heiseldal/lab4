package datastructure;

import java.lang.reflect.Array;

import javax.swing.CellEditor;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState state; 
    private CellState[][] matrise; 
    
    

    public CellGrid(int rows, int columns, CellState initialState) {
		if (rows < 0) {throw new IndexOutOfBoundsException();}
        else  this.rows = rows;
        if (columns < 0) {throw new IndexOutOfBoundsException();}
        else  this.columns = columns;
        this.state = initialState;
        this.matrise = new CellState[this.rows][this.columns];
        for (int i=0; i<rows;i++){
            for (int j=0; j<columns;j++){
                this.matrise[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row < this.rows){
            if(column < this.columns){
                this.matrise[row][column] = element;
            }
            else throw new IndexOutOfBoundsException();
        }
        else throw new IndexOutOfBoundsException();
        
    }

    @Override
    public CellState get(int row, int column) {
        if(row < this.rows){
            if(column < this.columns){
                return this.matrise[row][column];
            }
            else throw new IndexOutOfBoundsException();
        }
        else throw new IndexOutOfBoundsException();
    }

    @Override
    public IGrid copy(){
        CellGrid temp = new CellGrid(this.rows, this.columns, this.state);
        for (int i=0; i<this.rows;i++){
            for (int j=0; j<this.columns;j++){
                temp.set(i, j, this.matrise[i][j]);
            }
        }
        return temp;
    }
}
