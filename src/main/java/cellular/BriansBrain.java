package cellular;

import datastructure.IGrid;
import java.util.Random;
import datastructure.CellGrid;



public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int col) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, col);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		int row = nextGeneration.numRows();
		int col = nextGeneration.numColumns();
		for (int i=0; i<row;i++){
            for (int j=0; j<col;j++){
                currentGeneration.set(i, j, getNextCell(i, j));
            }
        }
        
    }

	public CellState getNextCell(int row, int col) {
		// TODO
		if(currentGeneration.get(row, col) == CellState.ALIVE){
            return CellState.DYING;
        }
        if (currentGeneration.get(row, col) == CellState.DYING){
            return CellState.DEAD;
        }
        if (currentGeneration.get(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE
        ) == 2){
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }


    private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;
		for (int i=row-1; i<=row+1;i++){
			if(i<0){}
			else if(i>numberOfRows()-1){}
			else{
				for (int j=col-1; j<=col+1;j++){
					if(j<0){}
					else if (j>numberOfColumns()-1){}
					else if (i == row && j== col){}
					else if (currentGeneration.get(i, j) == state){
						counter +=1;
					}
					
				}
			}
        }
		return counter;
    }


	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }
    
}
